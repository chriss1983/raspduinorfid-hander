package test.keysimulator;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;

import com.bqp.startapp.Main;

public class TestSimulator {
	public static void main(String[] args) {
		Thread t = new Thread(new KeySimulator());
//		new com.bqp.utils.MainController();
		new com.bqp.startapp.Main();

		t.start();
		
	}
	
	 public static void restart(String[] args) throws IOException, InterruptedException {
	        StringBuilder cmd = new StringBuilder();
	        cmd.append(System.getProperty("java.home") + File.separator + "bin" + File.separator + "java ");
	        for (String jvmArg : ManagementFactory.getRuntimeMXBean().getInputArguments()) {
	            cmd.append(jvmArg + " ");
	        }
	        cmd.append("-cp ").append(ManagementFactory.getRuntimeMXBean().getClassPath()).append(" ");
	        cmd.append(Main.class.getName()).append(" ");
	        for (String arg : args) {
	            cmd.append(arg).append(" ");
	        }
	        Runtime.getRuntime().exec(cmd.toString());
	        System.exit(0);
	    }
}
