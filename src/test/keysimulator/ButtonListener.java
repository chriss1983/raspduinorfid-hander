package test.keysimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JButton;

import com.bqp.controller.Controller;
import com.bqp.mainview.CardControllerFrame;

public class ButtonListener implements ActionListener {
	String buttonNames[];
	private Map<String, JButton> buttons;
	private String actions[] = { "BTN_LEFT", "BTN_LEFT_CENTER", "BTN_CENTER", "BTN_RIGHT_CENTER", "BTN_RIGHT" };

	public static String button = "";

	public ButtonListener(String buttonNames[], Map<String, JButton> buttons) {
		this.buttonNames = buttonNames;
		this.buttons = buttons;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		for (int i = 0; i < actions.length; i++) {
			if (src == buttons.get(buttonNames[i])) {
				CardControllerFrame.getInstance().makeAction(actions[i]);
			}
		}
	}

	public void addListener() {
		for (int i = 0; i < buttonNames.length; i++) {
			// buttons.get(buttonNames[i]).setActionCommand(actions[i]);
			buttons.get(buttonNames[i]).addActionListener(this);
		}
	}

}
