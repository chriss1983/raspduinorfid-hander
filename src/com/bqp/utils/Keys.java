package com.bqp.utils;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import javax.swing.JButton;

public class Keys {
	Robot r;
	private static Keys instance;
	
	public Keys() throws AWTException {
		this.r = new Robot();
	}
	
	public void close() {
		r.keyPress(KeyEvent.VK_ALT);
		r.keyPress(KeyEvent.VK_F4);
		r.keyRelease(KeyEvent.VK_ALT);
		r.keyRelease(KeyEvent.VK_F4);
	}
	
	public void up() {
		r.keyPress(KeyEvent.VK_UP);
		r.setAutoDelay(100);
		r.keyRelease(KeyEvent.VK_UP);
	}
	
	public void down() {
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
	}
	
	public void left() {
		r.keyPress(KeyEvent.VK_LEFT);
		r.keyRelease(KeyEvent.VK_LEFT);
	}

	public void right() {
		r.keyPress(KeyEvent.VK_RIGHT);
		r.keyRelease(KeyEvent.VK_RIGHT);
	}

	public void select() {
		r.keyPress(KeyEvent.VK_SPACE);
		r.keyRelease(KeyEvent.VK_SPACE);
	}
	
	public void tab() {
		r.keyPress(KeyEvent.VK_TAB);
		r.keyRelease(KeyEvent.VK_TAB);
	}

	public static Keys getInstance() throws AWTException {
		if(instance == null) {
			instance = new Keys();
		}
		return instance;
	}
	
	
}
