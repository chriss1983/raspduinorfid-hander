package com.bqp.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.ini4j.Config;
import org.ini4j.Ini;

import com.bqp.pojo.UserBaseTableEntity;

public class LoadProperties {
	private static LoadProperties instance;

	private Properties prop;
	private File iniFile = null;
	OutputStream output = null;
	private Map<Object, Object> map;
	
	private String file = "ressources/config.properties";
	private String ini = "ressources/config.ini";

	
	private LoadProperties() {
//		test();
	}

	public void test() {
		File file = new File(ini);
		Ini ini = new Ini();
		try {
			ini.load(new FileReader(file));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Map<String, String> map = ini.get("happy");
		
		System.out.println(map.get("age"));
		System.out.println(map.get("homeDir"));
		
		System.out.println(ini.get("currentlanguage", "lang"));
	    
		ini.put("currentlanguage","lang", "EN");
		try {
			ini.store(file);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public Map<String, String> getProperties(String className, String key) {
		Ini ini = loadFile(className);
		Map<String, String> map = ini.get(key);
		return map;
	}
	
	public String getValue(String className, String section, String key) {
		Ini ini = loadFile(className);
		return ini.get(section, key);
	}
	
	public void setLang(String ClassName, String lang) {
		Ini ini = loadFile(ClassName);
		ini.put("currentlanguage","lang", lang);
		try {
			ini.store(iniFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setValue(String className, String section, String key, String value) {
		Ini ini = loadFile(className);
		ini.put(section, key, value);
		try {
			ini.store(iniFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private Ini loadFile(String className) {
		iniFile = new File("ressources/" + className + ".ini");
		Ini ini = new Ini();
		try {
			ini.load(new FileReader(iniFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		}
		return ini;
	}

	public static LoadProperties getInstance() {
		if (instance == null) {
			instance = new LoadProperties();
		}
		return instance;
	}
}
