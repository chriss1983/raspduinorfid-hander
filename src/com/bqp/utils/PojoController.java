package com.bqp.utils;

import com.bqp.pojo.UserBaseTableEntity;
import com.bqp.pojo.UserDetailsEntity;
import com.bqp.pojo.UserLoginInfoEntity;

public class PojoController {
	private static PojoController instance;
	
	UserDetailsEntity userDetailsEntity = null;
	UserLoginInfoEntity userLoginInfoEntity = null;
	UserBaseTableEntity userBaseTableEntity = null;
	
	private PojoController() {
	}

	public UserDetailsEntity getUserDetailsEntity() {
		return userDetailsEntity;
	}

	public void setUserDetailsEntity(UserDetailsEntity userDetailsEntity) {
		this.userDetailsEntity = userDetailsEntity;
	}

	public UserLoginInfoEntity getUserLoginInfoEntity() {
		return userLoginInfoEntity;
	}

	public void setUserLoginInfoEntity(UserLoginInfoEntity userLoginInfoEntity) {
		this.userLoginInfoEntity = userLoginInfoEntity;
	}
	
	public UserBaseTableEntity getUserBaseTableEntity() {
		return userBaseTableEntity;
	}
	
	public void setUserBaseTableEntity(UserBaseTableEntity userBaseTableEntity) {
		this.userBaseTableEntity = userBaseTableEntity;
	}

	public static PojoController getInstance() {
		if(instance == null) {
			instance = new PojoController();
		}
		return instance;
	}
}
