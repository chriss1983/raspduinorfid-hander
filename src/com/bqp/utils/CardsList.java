package com.bqp.utils;

import java.util.HashMap;
import java.util.Map;

import com.bqp.views.menu.Menu;
import com.bqp.views.menu.RfidMainFrame;
import com.bqp.views.menu.TestCard3;
import com.bqp.views.menu.TestCard4;
import com.bqp.views.menu.absence.AbsenceReason;
import com.bqp.views.menu.settings.TestSubMenu1;
import com.bqp.views.menu.settings.clock.ClockSettings;
import com.bqp.views.menu.settings.clock.Selector;
import com.bqp.views.menu.settings.langselector.LanguageSelector;
import com.bqp.views.menu.settings.power.Power;

/**
 * 
 * @author chriss1983
 *
 *         In this property all the panels of the map are added, which are
 *         intended to be used as a menu.
 */
public interface CardsList {
	public String[] page = { "RFIDMAINFRAME", "ABSENCEREASON", "MENU", "POWER", "TESTCARD4", "LANGUAGESELECTOR",
			"CLOCKSETTINGS", "TESTSUBMENU1", "SELECTOR" };

	public Map<String, Object> PAGES = getPages();

	public static Map<String, Object> getPages() {
		Map<String, Object> pages = new HashMap<String, Object>();
		pages.put("RFIDMAINFRAME", RfidMainFrame.getInstance());
		pages.put("ABSENCEREASON", AbsenceReason.getInstance());
		pages.put("MENU", Menu.getInstance());
		pages.put("POWER", Power.getInstance());
		// pages.put("TESTCARD3", TestCard3.getInstance());
		pages.put("TESTCARD4", TestCard4.getInstance());
		pages.put("LANGUAGESELECTOR", LanguageSelector.getInstance());
		pages.put("TESTSUBMENU1", TestSubMenu1.getInstance());
		pages.put("CLOCKSETTINGS", ClockSettings.getInstance());
		pages.put("SELECTOR", Selector.getInstance());

		return pages;
	}

}
