package com.bqp.startapp;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;

import com.bqp.controller.Controller;
import com.bqp.mainview.CardControllerFrame;
import com.bqp.utils.connectors.SerialConnector;

public class Main implements Runnable {
	private CardControllerFrame layoutTest;
//	SerialConnector con = SerialConnector.getInstance();
	
	public static void main(String[] args) {
		Thread t = new Thread(new Main());
//		new com.bqp.utils.MainController();
		new test.keysimulator.KeySimulator();

		t.start();
	}

	public Main() {
		
		layoutTest = CardControllerFrame.getInstance();
		layoutTest.addObserver(Controller.getInstance());
	}
	
    public static void restart(String[] args) throws IOException, InterruptedException {
        StringBuilder cmd = new StringBuilder();
        cmd.append(System.getProperty("java.home") + File.separator + "bin" + File.separator + "java ");
        for (String jvmArg : ManagementFactory.getRuntimeMXBean().getInputArguments()) {
            cmd.append(jvmArg + " ");
        }
        cmd.append("-cp ").append(ManagementFactory.getRuntimeMXBean().getClassPath()).append(" ");
        cmd.append(Main.class.getName()).append(" ");
        for (String arg : args) {
            cmd.append(arg).append(" ");
        }
        Runtime.getRuntime().exec(cmd.toString());
        System.exit(0);
    }

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
}
