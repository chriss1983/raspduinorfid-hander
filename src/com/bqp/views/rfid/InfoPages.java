package com.bqp.views.rfid;

import java.util.HashMap;
import java.util.Map;

import com.bqp.views.rfid.pages.DefaultView;
import com.bqp.views.rfid.pages.ErrorView;
import com.bqp.views.rfid.pages.FailView;
import com.bqp.views.rfid.pages.InfoView;
import com.bqp.views.rfid.pages.InternalErrorView;
import com.bqp.views.rfid.pages.LoginView;
import com.bqp.views.rfid.pages.LogoutView;

/**
 * RaspDuino RFID Time Recording
 * @author Christian Richter
 * Date: 10.06.2015
 * Version: 1.0
 */

public interface InfoPages {
	public String[] page = { "DEFAULT", "LOGIN", "FAIL", "ERROR", "LOGOUT", "INTERNAL_ERROR", "INFO" };

	public Map<String, Object> PAGES = getPages();

	public static Map<String, Object> getPages() {
		Map<String, Object> pages = new HashMap<String, Object>();
		pages.put("DEFAULT", new DefaultView());
		pages.put("LOGIN", new LoginView());
		pages.put("FAIL", new FailView());
		pages.put("ERROR", new ErrorView());
		pages.put("LOGOUT", new LogoutView());
		pages.put("INTERNAL_ERROR", new InternalErrorView());
		pages.put("INFO", new InfoView());
		return pages;
	}

}
