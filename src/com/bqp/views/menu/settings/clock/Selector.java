package com.bqp.views.menu.settings.clock;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;

import com.bqp.utils.IButtons;
import com.bqp.views.menu.Cards;

public class Selector extends Cards implements IButtons {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Selector instance; 
	
	public Selector() {

//		this.setUndecorated(true);
//		this.setSize(200, 200); 
//		this.setLocationRelativeTo(null);
		
		JPanel p = new JPanel();
		
		p.setLayout(new BorderLayout());
		p.setBorder(getBorder());
		JLabel label = new JLabel("Das ist ein Test");
		p.add(label, BorderLayout.CENTER);
		
		add(p);
		
	}
	
	public JPanel init(){
		JPanel p = new JPanel(new BorderLayout());
		JLabel lblInfo = new JLabel();
		lblInfo.setText("Hallo Welt");
		p.add(lblInfo);
		return p;
	}
	
//	private Border getBorder() {
//		return  BorderFactory.createLineBorder(Color.red);
//	}

	public void showWindow() {
		this.setVisible(true);
	}
	
//	public void closeWindow() {
//		this.dispose();
//	}
	
	private void showDialog() {
		int n = JOptionPane.showConfirmDialog(null,
                "I really like my book",
                "Question (application-modal dialog)", 
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE); 
        System.out.println(n); // Use n for response

	}
	
	public static Selector getInstance() {
		if(instance == null) {
			instance = new Selector();
		}
		return instance;
	}

	@Override
	public void BTN_LEFT() {
		showDialog();
	}

	@Override
	public void BTN_LEFT_CENTER() {

	}

	@Override
	public void BTN_CENTER() {

	}

	@Override
	public void BTN_RIGHT_CENTER() {

	}

	@Override
	public void BTN_RIGHT() {

	}

}
