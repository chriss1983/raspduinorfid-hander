package com.bqp.views.menu.settings.clock;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.bqp.mainview.CardControllerFrame;
import com.bqp.utils.IButtons;
import com.bqp.utils.LangUpdate;
import com.bqp.utils.LoadProperties;
import com.bqp.utils.view.ColoredJButton;
import com.bqp.views.menu.Cards;
import com.bqp.views.menu.Menu;
import com.bqp.views.menu.settings.langselector.Selector;

public class ClockSettings extends Cards implements IButtons {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static ClockSettings instance;
	private Selector selector;
	private Box hBox;
	private JPanel rightPanel;
	private int selectedButton = 0;

	public ClockSettings() {
		setTitle(getClass().getSimpleName().toUpperCase());
		// this.setBackground(Color.BLUE);
		add(new JLabel(getClass().getName()), BorderLayout.CENTER);

		// Box vBox = Box.createVerticalBox();
		JPanel p = new JPanel();
		Iterator<?> it = addButtons().iterator();

		// while (it.hasNext()) {
		// JButton button = (JButton) it.next();
		// vBox.add(button);
		// // vBox.add(Box.createVerticalGlue());
		// }
		rightPanel = new JPanel();

		rightPanel.setBackground(Color.BLUE);
		rightPanel.setLayout(new GridLayout(0, 1));
		rightPanel.add(Box.createVerticalGlue());

		JPanel panel = new JPanel(new BorderLayout());
		while (it.hasNext()) {

			JButton button = (JButton) it.next();
			rightPanel.add(button);
		}
		panel.add(rightPanel, BorderLayout.EAST);
		panel.add(rightPanel);
		rightPanel.add(Box.createVerticalGlue());
		// add(vBox, BorderLayout.CENTER);
		add(rightPanel, BorderLayout.EAST);
		setLabels();
	}
	
	private List<ColoredJButton> addButtons() {
		List<ColoredJButton> buttons = new ArrayList<ColoredJButton>();
		Map<String, String> map = new HashMap<>();
		String clazz = this.getClass().getSimpleName().toLowerCase();
		map = LoadProperties.getInstance().getProperties(getClassName(), this.getClass().getSimpleName().toLowerCase());

		System.out.println("Get Language: " + getLang());
		
		for (Map.Entry<String, String> entry : map.entrySet()) {
			System.out.println(clazz + "." + entry.getKey().substring(entry.getKey().lastIndexOf('.')+1, entry.getKey().length()));
			if (entry.getKey().equals("clocksettings." + getLang())) {
				
				ColoredJButton button = new ColoredJButton();
//				if (entry.getKey().substring(7, entry.getKey().length()-1).equals("menu." + getLang())) {
				System.out.println(clazz + "." + getLang() + "." + entry.getKey().substring(entry.getKey().lastIndexOf('.') +1, entry.getKey().length()));
				System.out.println("Data: " + clazz + "." + getLang() + "." + entry.getKey().substring(entry.getKey().lastIndexOf('.') +1, entry.getKey().length()));
				System.out.println("ACTIONCMD: " + entry.getKey().substring(entry.getKey().lastIndexOf('.') + 1 , entry.getKey().length()));
				button.setText(map.get(clazz + "." + getLang() + "." + entry.getKey().substring(entry.getKey().lastIndexOf('.')+1, entry.getKey().length())));
				button.setActionCommand(entry.getKey().substring(entry.getKey().lastIndexOf('.') + 1, entry.getKey().length()));
				

				button.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						System.out.println("Hier ist Button " + e.getActionCommand());
						Iterator<?> it = buttons.iterator();

						menuActionMethod(e.getActionCommand());

						if (e.getActionCommand().equals(entry.getValue())) {
							LoadProperties.getInstance().setLang(getClassName(), "DE");
							setLabels();
						} else if (e.getActionCommand().equals("Lang EN")) {
							LoadProperties.getInstance().setLang(getClassName(), "EN");
							setLabels();
						}
					}
				});
				buttons.add(button);
			}
		}
		return buttons;
	}
	
	private void menuActionMethod(String method) {
		// Step1 - Using string funClass to convert to class
		// String funClass = "package.myclass";
		String actionClass = LoadProperties.getInstance().getValue(getClassName(), "menubuttons", "menu.actionclass");
		Class<?> c = null;
		try {
			c = Class.forName(actionClass);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Step2 - instantiate an object of the class abov
		Object o = null;
		try {
			o = c.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Prepare array of the arguments that your function accepts, lets
		// say only one string here
		Class[] paramTypes = new Class[1];
		// paramTypes[0] = String.class;
		// String methodName = "mymethod";
		// Instantiate an object of type method that returns you method name
		Method m = null;
		try {
			m = c.getDeclaredMethod(method, null);
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// invoke method with actual params
		try {
			m.invoke(o, null);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void BTN_LEFT() {
		CardControllerFrame.getInstance().setAction("MENU");
		CardControllerFrame.getInstance().setMenu("MENU");
	}

	@Override
	public void BTN_LEFT_CENTER() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void BTN_CENTER() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void BTN_RIGHT_CENTER() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void BTN_RIGHT() {
		// TODO Auto-generated method stub
	}
	
	public static ClockSettings getInstance() {
		if(instance == null) {
			instance = new ClockSettings();
		}
		return instance;
	}
}
