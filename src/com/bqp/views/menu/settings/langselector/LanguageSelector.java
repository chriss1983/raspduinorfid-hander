package com.bqp.views.menu.settings.langselector;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;

import com.bqp.mainview.CardControllerFrame;
import com.bqp.startapp.Main;
import com.bqp.utils.CardsList;
import com.bqp.utils.IButtons;
import com.bqp.utils.LangUpdate;
import com.bqp.utils.LoadProperties;
import com.bqp.views.menu.Cards;
import com.bqp.views.menu.Menu;
import com.bqp.views.menu.RfidMainFrame;

public class LanguageSelector extends Cards implements IButtons, LangUpdate {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static LanguageSelector instance;
	private Selector selector;

	private LanguageSelector() {
		selector = new Selector();
		setTitle(getClass().getSimpleName().toUpperCase());
		add(new JLabel(getClass().getName()), BorderLayout.CENTER);

		Box vBox = Box.createVerticalBox();
//		Iterator<?> it = addButtons().iterator();
//
//		while (it.hasNext()) {
//			JButton button = (JButton) it.next();
//			vBox.add(button);
//			// vBox.add(Box.createVerticalGlue());
//		}
		add(vBox);
		setLabels();
	}

	private void updateLabelsLang() {
		for (int i = 0; i < CardsList.PAGES.size(); i++) {
			// Step1 - Using string funClass to convert to class
			// String funClass = "package.myclass";
			Class<?> c = null;
			try {
				c = Class.forName(CardsList.PAGES.get(CardsList.page[i]).getClass().getName());
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Step2 - instantiate an object of the class abov
			Object o = null;
			try {
				o = c.newInstance();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Prepare array of the arguments that your function accepts, lets
			// say only one string here
			Class[] paramTypes = new Class[1];
			// paramTypes[0] = String.class;
			// String methodName = "mymethod";
			// Instantiate an object of type method that returns you method name
			Method m = null;
			try {
				m = c.getDeclaredMethod("updateLabels", null);
			} catch (NoSuchMethodException | SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// invoke method with actual params
			try {
				m.invoke(o, null);
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private List<JButton> addButtons() {
		List<JButton> buttons = new ArrayList<JButton>();

		buttons.add(new JButton("Lang DE"));
		buttons.add(new JButton("Lang EN"));

		Iterator<?> it = buttons.iterator();

		while (it.hasNext()) {
			JButton button = (JButton) it.next();
			button.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					System.out.println("Hier ist Button " + e.getActionCommand());
					if (e.getActionCommand().equals("Lang DE")) {
						LoadProperties.getInstance().setLang(getClassName(), "DE");
						setLabels();
					} else if (e.getActionCommand().equals("Lang EN")) {
						LoadProperties.getInstance().setLang(getClassName(), "EN");
						setLabels();
					}
				}
			});
		}
		return buttons;
	}

	@Override
	public void BTN_LEFT() {
		System.out.println("BTN_LEFT " + getClass().getSimpleName());
		CardControllerFrame.getInstance().setAction("MENU");
		CardControllerFrame.getInstance().setMenu("MENU");
		setLabels();
	}

	@Override
	public void BTN_LEFT_CENTER() {
//		selector.closeWindow();
		System.out.println("BTN_LEFT_CENTER " + getClass().getSimpleName());
		List<String> lblText = new ArrayList<String>();

		lblText.add("A");
		lblText.add("B");
		lblText.add("C");
		lblText.add("D");
		lblText.add("E");

		setLabelText(lblText);
	}

	@Override
	public void BTN_CENTER() {
		System.out.println("BTN_CENTER " + getClass().getSimpleName());
		setLanguage("PL");
	}

	@Override
	public void BTN_RIGHT_CENTER() {
		System.out.println("BTN_RIGHT_CENTER " + getClass().getSimpleName());
		setLanguage("EN");
	}

	@Override
	public void BTN_RIGHT() {
		setLanguage("DE");
	}

	@Override
	public void updateLabels() {
		System.out.println("LANGUPDATE LANGSELECTOR");
		setLabels();
		// CardControllerFrame.getInstance().restart();
	}

	private void setLanguage(String lang) {
		System.out.println("AKTUELLE SPRACHE = " + getLang());
		// setLabels();
		System.out.println("Lang = " + LoadProperties.getInstance().getValue("generalConfig", "currentlanguage", "lang"));
//		if (!langInIni.equals(lang)) {
			LoadProperties.getInstance().setLang("generalConfig", lang);
			
			selector.showWindow();
//			updateLabelsLang();
			try {
				Thread.sleep(2000);
				String[] args = { "java", "Main" };
				Main.restart(args);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// CardControllerFrame.getInstance().restart();
//		}
	}

	public static LanguageSelector getInstance() {
		if (instance == null) {
			instance = new LanguageSelector();
		}
		return instance;
	}
}
