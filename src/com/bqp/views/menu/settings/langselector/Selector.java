package com.bqp.views.menu.settings.langselector;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import com.bqp.utils.IButtons;
import com.bqp.views.menu.Cards;

public class Selector extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Selector() {
		this.setUndecorated(true);
		this.setSize(200, 200); 
		this.setLocationRelativeTo(null);
		
		JPanel p = new JPanel();
		
		p.setLayout(new BorderLayout());
		p.setBorder(getRedBorder());
		JLabel label = new JLabel("Das ist ein Test");
		p.add(label, BorderLayout.CENTER);
		
		add(p);
	}
	
	public JPanel init(){
		JPanel p = new JPanel(new BorderLayout());
		JLabel lblInfo = new JLabel();
		lblInfo.setText("Halloe Welt");
		p.add(lblInfo);
		return p;
	}
	
	private Border getRedBorder() {
		return  BorderFactory.createLineBorder(Color.red);
	}

	public void showWindow() {
		this.setVisible(true);
	}
	
	public void closeWindow() {
		this.dispose();
	}
}
