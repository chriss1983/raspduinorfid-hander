package com.bqp.views.menu.settings;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;

import com.bqp.utils.IButtons;
import com.bqp.utils.LangUpdate;
import com.bqp.views.menu.Cards;
import com.bqp.views.menu.RfidMainFrame;

public class TestSubMenu1 extends Cards implements IButtons, LangUpdate {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static TestSubMenu1 instance;

	public TestSubMenu1() {
		this.setTitle(getClass().getSimpleName().toUpperCase());
		this.setBackground(Color.GREEN);
		this.setLayout(new BorderLayout());

		Box vBox = Box.createVerticalBox();

		Iterator<?> it = addButtons().iterator();

		while (it.hasNext()) {
			JButton button = (JButton) it.next();
			vBox.add(button);
			// vBox.add(Box.createVerticalGlue());
		}
		add(new JLabel(getClass().getName()), BorderLayout.CENTER);
//		add(vBox, BorderLayout.EAST);
		add(getBorderPanel(), BorderLayout.SOUTH);
		

	}

	private List<JButton> addButtons() {
		List<JButton> buttons = new ArrayList<JButton>();

		buttons.add(new JButton("BUTTON 1"));
		buttons.add(new JButton("BUTTON 2"));
		buttons.add(new JButton("BUTTON 3"));
		buttons.add(new JButton("BUTTON 4"));
		buttons.add(new JButton("BUTTON 5"));

		Iterator<?> it = buttons.iterator();

		while (it.hasNext()) {
			JButton button = (JButton) it.next();
			button.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					System.out.println("Hier ist Button " + e.getActionCommand());
				}
			});
		}
		return buttons;
	}

	@Override
	public void BTN_LEFT() {
		System.out.println("BTN_LEFT from TestMenu");
	}

	@Override
	public void BTN_LEFT_CENTER() {
		System.out.println("BTN_LEFT_CENTER from TestMenu");
	}

	@Override
	public void BTN_CENTER() {
		System.out.println("BTN_CENTER from TestMenu");
	}

	@Override
	public void BTN_RIGHT_CENTER() {
		System.out.println("BTN_RIGHT_CENTER from TestMenu");

	}

	@Override
	public void BTN_RIGHT() {
		System.out.println("BTN_RIGHT from TestMenu");
		List<String> lblText = new ArrayList<String>();

		lblText.add("Test");
		lblText.add("Bla");
		lblText.add("Tataaaa");
		lblText.add("");
		lblText.add("Muh");

		setLabelText(lblText);
	}

	@Override
	public void updateLabels() {
		// TODO Auto-generated method stub
	}
	
	public static TestSubMenu1 getInstance() {
		if(instance == null) {
			instance = new TestSubMenu1();
		}
		return instance;
	}
}
