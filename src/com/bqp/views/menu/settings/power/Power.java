package com.bqp.views.menu.settings.power;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.bqp.mainview.CardControllerFrame;
import com.bqp.utils.IButtons;
import com.bqp.utils.Keys;
import com.bqp.utils.LoadProperties;
import com.bqp.utils.view.ColoredJButton;
import com.bqp.views.menu.Cards;

public class Power extends Cards implements IButtons {

	private static final long serialVersionUID = 1L;
	private int selectedButton = 0;
	private JPanel rightPanel;
	private static Power instance;

	public Power() {
		setTitle(getClass().getSimpleName().toUpperCase());
		add(new JLabel(getClass().getName()), BorderLayout.CENTER);

		JPanel p = new JPanel();
		Iterator<?> it = getButtons().iterator();
		rightPanel = new JPanel();
		rightPanel.setBackground(Color.BLACK);
		rightPanel.setLayout(new GridLayout(0, 1));
		rightPanel.add(Box.createVerticalGlue());

		JPanel panel = new JPanel(new BorderLayout());
		while (it.hasNext()) {

			JButton button = (JButton) it.next();
			rightPanel.add(button);
		}
		panel.add(rightPanel, BorderLayout.EAST);
		panel.add(rightPanel);
		rightPanel.add(Box.createVerticalGlue());
		add(rightPanel, BorderLayout.EAST);
		setLabels();
	}
	
	@Override
	public void BTN_LEFT() {
		CardControllerFrame.getInstance().setAction("RFIDMAINFRAME");
		CardControllerFrame.getInstance().setMenu("RFIDMAINFRAME");
	}

	@Override
	public void BTN_LEFT_CENTER() {

	}

	@Override
	public void BTN_CENTER() {

	}

	@Override
	public void BTN_RIGHT_CENTER() {
		CardControllerFrame.getInstance().setFocus();
		try {
			Keys.getInstance().select();
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void BTN_RIGHT() {
		CardControllerFrame.getInstance().setFocus();

		if (getButtons().get(1).isFocusable()) {
			System.out.println("SELECTED  BUTTON = " + getButtons().get(1));
		}

		if (selectedButton < getButtons().size()) {
			getButtons().get(selectedButton).setSelected(true);
			if (selectedButton > 0) {
				getButtons().get(selectedButton).setSelected(true);
			}
		} else {
			selectedButton = 0;
		}

		try {
			Keys.getInstance().tab();
			// Keys.getInstance().up();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Power getInstance() {
		if (instance == null) {
			instance = new Power();
		}
		return instance;
	}

}
