package com.bqp.views.menu;

import java.awt.Color;

import javax.swing.JPanel;

import com.bqp.controller.Controller;
import com.bqp.mainview.CardControllerFrame;
import com.bqp.utils.IButtons;
import com.bqp.utils.LangUpdate;

public class TestCard3 extends Cards implements IButtons, LangUpdate  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static TestCard3 instance;

	public TestCard3() {
		setTitle(getClass().getSimpleName().toUpperCase());
		this.setBackground(Color.RED);
	}

	@Override
	public void BTN_LEFT() {
		System.out.println("BTN_LEFT " + getClass().getSimpleName());
	}
	
	@Override
	public void BTN_LEFT_CENTER() {
		System.out.println("BTN_LEFT_CENTER " + getClass().getSimpleName());
	}

	@Override
	public void BTN_CENTER() {
		System.out.println("BTN_CENTER " + getClass().getSimpleName());		
	}

	@Override
	public void BTN_RIGHT_CENTER() {
		System.out.println("BTN_RIGHT_CENTER " + getClass().getSimpleName());				
	}

	@Override
	public void BTN_RIGHT() {
		System.out.println("BTN_RIGHT " + getClass().getSimpleName());	
	}

	@Override
	public void updateLabels() {
		System.out.println("LANGUPDATE TESTCARD");
		setLabels();
	}
	
	public static TestCard3 getInstance() {
		if(instance == null) {
			instance = new TestCard3();
		}
		return instance;
	}
}
