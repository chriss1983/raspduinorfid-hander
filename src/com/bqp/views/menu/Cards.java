package com.bqp.views.menu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import com.bqp.controller.Controller;
import com.bqp.utils.CardsList;
import com.bqp.utils.IButtons;
import com.bqp.utils.LoadProperties;
import com.bqp.utils.view.ColoredJButton;
import com.bqp.views.general.DigitalClock;
import com.bqp.views.general.ImageLoader;
import com.bqp.views.menu.settings.clock.ClockSettings;

public abstract class Cards extends JPanel implements IButtons {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Box hBox;
	protected JPanel borderedPanel;
	private JPanel soutPanel;
	private JPanel infoPanel;
	private DigitalClock clock;
	private JLabel lblInfo;
	private TextArea textArea;

	public Cards() {
		this.setLayout(new BorderLayout());
		soutPanel = new JPanel(new BorderLayout());
		infoPanel = new JPanel(new BorderLayout());
		Box hBox = Box.createHorizontalBox();
		hBox.add(Box.createHorizontalGlue());
		lblInfo = new JLabel("Bereit");
		hBox.add(lblInfo);
		infoPanel.add(hBox);

		if (setClockEnabled()) {
			clock = new DigitalClock();
			clock.start();
			soutPanel.add(clock, BorderLayout.NORTH);
		}
		soutPanel.add(getBorderPanel(), BorderLayout.CENTER);
		soutPanel.add(infoPanel, BorderLayout.SOUTH);
		try {
			add(new ImageLoader(), BorderLayout.NORTH);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.add(soutPanel, BorderLayout.SOUTH);
		textArea = new TextArea();
		textArea.setFocusable(false);
		this.add(textArea, BorderLayout.CENTER);
		
	}

	/**
	 * sets standard designations for the label of the Button Border
	 */
	private List<JLabel> labels = new ArrayList<JLabel>() {
		private static final long serialVersionUID = 1L;

		{
			add(new JLabel("lblLeft"));
			add(new JLabel("lblLeftCenter"));
			add(new JLabel("lblCenter"));
			add(new JLabel("lblRightCenter"));
			add(new JLabel("lblRightCenter"));
		}
	};

	/** Test Gui Console */
	public void updateTextArea(final String text) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				textArea.append(text);
			}
		});
	}

	public void redirectSystemStreams() {
		OutputStream out = new OutputStream() {
			@Override
			public void write(int b) throws IOException {
				updateTextArea(String.valueOf((char) b));
			}

			@Override
			public void write(byte[] b, int off, int len) throws IOException {
				updateTextArea(new String(b, off, len));
			}

			@Override
			public void write(byte[] b) throws IOException {
				write(b, 0, b.length);
			}
		};

		System.setOut(new PrintStream(out, true));
		System.setErr(new PrintStream(out, true));
	}

	/** End Test Gui Console */

	/**
	 * @return provides a back panel set containing a Title Border set
	 *         containing the heading of the Buttons
	 */
	public JPanel getBorderPanel() {
		borderedPanel = new JPanel(new BorderLayout());
		Border bGreyLine = BorderFactory.createLineBorder(Color.GRAY, 1, false);
		Border bTitled1 = BorderFactory.createTitledBorder(bGreyLine, "Menu", TitledBorder.CENTER, TitledBorder.TOP);
		borderedPanel.setBorder(bTitled1);
		borderedPanel.add(getLabels());
		return borderedPanel;
	}

	/**
	 * Executes the passed method, which is passed as a string in which
	 * inherited from Card object, which were defined in the Interface
	 * {@code CardList}, these methods are in the interface {@code IButtons}
	 * from. The methods must be implemented in the inherited property in order
	 * to ensure the function.
	 * 
	 * @see IButtons
	 * @see CardsList
	 * 
	 * @param String
	 *            value that corresponds to the name of the method in the
	 *            inherited property
	 * @throws ClassNotFoundException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @see IButtons
	 */
	public void doAction(String action) throws ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Class<?> cards = Class.forName(getClass().getName());
		Class<?> noparams[] = {};
		// For Arguments
		// Class[] argTypes = new Class[] { String[].class };
		Method method;

		while (Controller.action.equals(getClass().getSimpleName().toUpperCase())) {
			/** Call Methods per Reflection */
			if (!Controller.button.equals("")) {
				System.out.println("ClassNAME: " + getClass().getSimpleName().toLowerCase());
				System.out.println("Button " + Controller.button);
				method = cards.getDeclaredMethod(Controller.button, noparams);
				method.invoke(this);
				Controller.button = "";
			}
		}
	}

	/**
	 * Reads the values from the .ini file that is associated with the inherited
	 * class and loads from this all the key and value pairs to a
	 * {@code Map<String, String>} by which the values can be easily read.
	 * 
	 * After reading out the values on the get method of the
	 * {@code Map<String, String>} are entered in the label, to the already
	 * defined methods {@code getClassName()} and {@code getLang} be used.
	 * 
	 * At the end of the method {@code getLang} the string is still appended,
	 * which the corresponding button Represents.
	 * 
	 * 
	 * In order to read the auxiliary class {@code LoadProperties} is used,
	 * which contains all the necessary methods to reading all necessary values
	 * from the ini files read and write. Furthermore, it should be noted that
	 * is the class {@code LoadProperties} a singleton.
	 * 
	 * @see LoadProperties
	 * 
	 */
	public void setLabels() {
		List<String> lblText = new ArrayList<String>();
		Map<String, String> map = new HashMap<>();
		map = LoadProperties.getInstance().getProperties(getClassName(), getClassName());
		System.out.println("Get Language: " + getLang());
		lblText.add(map.get(getClassName() + "." + getLang() + ".left"));
		lblText.add(map.get(getClassName() + "." + getLang() + ".leftCenter"));
		lblText.add(map.get(getClassName() + "." + getLang() + ".center"));
		lblText.add(map.get(getClassName() + "." + getLang() + ".rightCenter"));
		lblText.add(map.get(getClassName() + "." + getLang() + ".right"));
		setLabelText(lblText);
	}

	/**
	 * @return adds all the labels in a horizontal box, and returns them
	 */
	protected Box getLabels() {
		hBox = Box.createHorizontalBox();
		Iterator<JLabel> it = labels.iterator();
		while (it.hasNext()) {
			JLabel label = (JLabel) it.next();
			label.setFont(new Font("Georgia", Font.BOLD, 14));
			hBox.add(Box.createHorizontalGlue());
			hBox.add(label);
			label.setForeground(Color.WHITE);
			label.setOpaque(true);
			label.setBackground(Color.DARK_GRAY);
			hBox.add(Box.createHorizontalGlue());
		}
		return hBox;
	}

	/**
	 * 
	 * @return Returns the current object name back to lowercase. Here, a simple
	 *         name is generated from the fully qualified name. As an example:
	 *         from {@codejava.util.lang.String} {@code string} is
	 */
	public String getClassName() {
		return this.getClass().getSimpleName().toLowerCase();
	}

	/**
	 * 
	 * @return
	 */
	public List<ColoredJButton> getButtons() {
		List<ColoredJButton> buttons = new ArrayList<ColoredJButton>();
		Map<String, String> map = new HashMap<>();
		String clazz = this.getClass().getSimpleName().toLowerCase();
		map = LoadProperties.getInstance().getProperties(getClassName(), clazz);
		System.out.println("ACTIONCLASS " + LoadProperties.getInstance().getValue(getClassName(), getClassName(),
				getClassName() + ".actionclass"));

		for (Map.Entry<String, String> entry : map.entrySet()) {
			if (entry.getKey().substring(0, getClassName().length() + 3).equals(getClassName() + ".id")) {
				ColoredJButton button = new ColoredJButton();
				System.out.println("Data: " + clazz + "." + getLang() + "."
						+ entry.getKey().substring(entry.getKey().lastIndexOf('.') + 1, entry.getKey().length()));
				System.out.println("ACTIONCMD: "
						+ entry.getKey().substring(entry.getKey().lastIndexOf('.') + 1, entry.getKey().length()));
				button.setText(map.get(clazz + "." + getLang() + "."
						+ entry.getKey().substring(entry.getKey().lastIndexOf('.') + 1, entry.getKey().length())));
				button.setActionCommand(
						entry.getKey().substring(entry.getKey().lastIndexOf('.') + 1, entry.getKey().length()));
				button.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						invokeActionMethod(e.getActionCommand(), LoadProperties.getInstance().getValue(getClassName(),
								getClassName(), getClassName() + ".actionclass"));
					}
				});
				buttons.add(button);
			}
		}
		return buttons;
	}

	/**
	 * Reflection leads via the given method in the given object. This method is
	 * used to bond them as ActionEvent to the corresponding buttons of the
	 * menu, so as to involve at a later date dynamic new buttons on the .ini
	 * file, without the whole project again to recompile.
	 * 
	 * In the Interface Cards List all objects are defined which use this method
	 * 
	 * @see CardsList
	 * 
	 * @param method
	 * @param actionClass
	 */
	public void invokeActionMethod(String method, String actionClass) {
		Class<?> c = null;
		try {
			c = Class.forName(actionClass);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Object o = null;
		try {
			o = c.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Method m = null;
		try {
			m = c.getDeclaredMethod(method, null);
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		try {
			m.invoke(o, null);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Reads the Boolean out as a string of the appropriate .ini file and
	 * returns it as a boolean.
	 * 
	 * @see ClockSettings
	 * 
	 * @return This method can be checked whether the clock should be displayed
	 *         or not.
	 */
	public Boolean setClockEnabled() {
		return Boolean.parseBoolean(
				LoadProperties.getInstance().getValue("generalConfig", "clocksettings", "clock.setenabled"));
	}

	/**
	 * @return Returns the language value from the .ini file which is registered
	 *         as a single letter as 'DE' or 'EN'
	 */
	public String getLang() {
		return LoadProperties.getInstance().getValue("generalConfig", "currentlanguage", "lang");
	}

	/**
	 * @param title
	 *            sets the title of the panel
	 * @return
	 */
	public String setTitle(String title) {
		return title;
	}

	/**
	 * Changes the already entered text in label panel, while the values from a
	 * list are read and entered, if a value in this list is empty (not null),
	 * this is skipped. This makes it possible to hide unneeded labels while
	 * runtime.
	 * 
	 * @param List<String>
	 *            It is expected a list of type String, which contains five
	 *            string messages.
	 */
	public void setLabelText(List<String> lblText) {
		Iterator<JLabel> label = labels.iterator();
		Iterator<String> txt = lblText.iterator();

		while (label.hasNext() && txt.hasNext()) {
			JLabel jLabel = (JLabel) label.next();
			String string = (String) txt.next();
			if (string != null) {
				jLabel.setText(string);
			}
		}
	}

	public void setInfoLbl(String info, int delay) {
		lblInfo.setText(info);
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(delay);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				lblInfo.setText(LoadProperties.getInstance().getValue("generalConfig", "infolbl",
						"info." + getLang() + ".label"));
			}
		}).start();
	}
}
