package com.bqp.views.menu;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.bqp.utils.constants.Colors;
import com.bqp.views.general.DigitalClock;
import com.bqp.views.rfid.InfoPages;
import com.bqp.views.rfid.pages.LoginView;
import com.bqp.views.rfid.pages.LogoutView;

public class RfidMainFrame extends Cards implements Colors {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel mainPanel;
	private JPanel southPanel;
	public static JPanel cardLayout;
	private JPanel dataContainer = new JPanel();
	private static String settingData;
	private static RfidMainFrame instance;

	public RfidMainFrame() {
//		this.setTitle(getClass().getSimpleName().toUpperCase());
		// this.setBackground(Color.YELLOW);

		Box vBox = Box.createVerticalBox();
		Iterator<?> it = addButtons().iterator();

		while (it.hasNext()) {
			JButton button = (JButton) it.next();
			vBox.add(button);
		}
		// add(new JLabel(getClass().getName()), BorderLayout.CENTER);
		try {
			initGui();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// add(getBorderPanel(), BorderLayout.SOUTH);
		// this.add(getBorderPanel());
		 setLabels();
		redirectSystemStreams();

	}

	private void initGui() throws IOException {

		/** Create a Main-Panel for any Components */
		mainPanel = new JPanel(new BorderLayout());
		southPanel = new JPanel(new BorderLayout());

		/** Add a new CardLayout to mainPanel */
		cardLayout = new JPanel(new CardLayout());
		cardLayout.add(new JLabel(getClass().getSimpleName().toUpperCase()), BorderLayout.CENTER);

		/** set Forground and Background-Color to White */
		mainPanel.setBackground(bgColor); // set BackgroundColor
		mainPanel.setForeground(fgColor); // set ForgroundColor

		mainPanel.add(dataContainer);
		mainPanel.add(cardLayout, BorderLayout.CENTER);
		this.add(mainPanel);
		System.out.println(this.getClass().getSimpleName().toLowerCase());
		setLabels();
	}
	
	public void setPage(String page) {
		// logger.debug("Called Page : " + page);

		if (page.contains("&")) {
			/** Split by '&' */
			String cmd[] = page.split("&"); 
			/** Save String before '&' to 'page' String */
			page = cmd[0];
			/** Save String after '&' to 'userName' */
			settingData = cmd[1];
			// logger.info("Call Page : " + page + "\nUsername : " + userName);
			if (InfoPages.PAGES.get(page) instanceof LoginView) {
				((LoginView) InfoPages.PAGES.get(page)).setUserName(settingData);
			} else if (InfoPages.PAGES.get(page) instanceof LogoutView) {
				((LogoutView) InfoPages.PAGES.get(page)).setUserName(settingData);
			}
		}

		CardLayout cards = (CardLayout) cardLayout.getLayout();
		cards.show(cardLayout, page);
		// mainPanel.repaint();
	}

	private List<JButton> addButtons() {
		List<JButton> buttons = new ArrayList<JButton>();

		buttons.add(new JButton("BUTTON 1"));
		buttons.add(new JButton("BUTTON 2"));
		buttons.add(new JButton("BUTTON 3"));
		buttons.add(new JButton("BUTTON 4"));
		buttons.add(new JButton("BUTTON 5"));

		Iterator<?> it = buttons.iterator();

		while (it.hasNext()) {
			JButton button = (JButton) it.next();
			button.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					System.out.println("Hier ist Button " + e.getActionCommand());
				}
			});
		}
		return buttons;
	}

	@Override
	public void BTN_LEFT() {
		setLabels();
		System.out.println("BTN_LEFT " + getClass().getSimpleName());
		RfidMainFrame.getInstance().setPage("DEFAULT");
	}

	@Override
	public void BTN_LEFT_CENTER() {
		setLabels();
		RfidMainFrame.getInstance().setPage("ERROR");
	}

	@Override
	public void BTN_CENTER() {
		setLabels();
		RfidMainFrame.getInstance().setPage("LOGIN&Christian Richter");
	}

	@Override
	public void BTN_RIGHT_CENTER() {
		setLabels();
		RfidMainFrame.getInstance().setPage("LOGOUT&Christian Richter");
	}

	@Override
	public void BTN_RIGHT() {
		setLabels();
		// CardControllerFrame.getInstance().setAction("");
		// CardControllerFrame.getInstance().setMenu("");
		// CardControllerFrame.getInstance().makeAction("MENU " +
		// getClass().getSimpleName());
		System.out.println("BTN_RIGHT");
	}

	public static RfidMainFrame getInstance() {
		if (instance == null) {
			instance = new RfidMainFrame();
		}
		return instance;
	}
}
