package com.bqp.views.menu;

import java.awt.Color;

import javax.swing.JPanel;

import com.bqp.utils.IButtons;
import com.bqp.utils.LangUpdate;

public class TestCard4 extends Cards implements IButtons, LangUpdate  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static TestCard4 instance;

	public TestCard4() {
		this.setTitle(getClass().getSimpleName().toUpperCase());
		this.setBackground(Color.BLACK);
	}
	
	@Override
	public void BTN_LEFT() {
		System.out.println("BTN_LEFT " + getClass().getSimpleName());
	}
	
	@Override
	public void BTN_LEFT_CENTER() {
		System.out.println("BTN_LEFT_CENTER " + getClass().getSimpleName());
	}

	@Override
	public void BTN_CENTER() {
		System.out.println("BTN_CENTER " + getClass().getSimpleName());		
	}

	@Override
	public void BTN_RIGHT_CENTER() {
		System.out.println("BTN_RIGHT_CENTER " + getClass().getSimpleName());				
	}

	@Override
	public void BTN_RIGHT() {
		System.out.println("BTN_RIGHT " + getClass().getSimpleName());	
	}

	@Override
	public void updateLabels() {
		// TODO Auto-generated method stub
	}
	
	public static TestCard4 getInstance() {
		if(instance == null) {
			instance = new TestCard4();
		}
		return instance;
	}
}
