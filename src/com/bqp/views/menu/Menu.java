package com.bqp.views.menu;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.bqp.mainview.CardControllerFrame;
import com.bqp.utils.IButtons;
import com.bqp.utils.Keys;
import com.bqp.utils.LangUpdate;
import com.bqp.utils.LoadProperties;
import com.bqp.utils.view.ColoredJButton;

public class Menu extends Cards {
	private static final long serialVersionUID = 1L;
	private static Menu instance;
	private JPanel rightPanel;
	private int selectedButton = 0;

	public Menu() {
		setTitle(getClass().getSimpleName().toUpperCase());
//		add(new JLabel(getClass().getName()), BorderLayout.CENTER);
		Iterator<?> it = getButtons().iterator();
		rightPanel = new JPanel();
		rightPanel.setBackground(Color.BLACK);
		rightPanel.setLayout(new GridLayout(0, 1));
		rightPanel.add(Box.createVerticalGlue());
		JPanel panel = new JPanel(new BorderLayout());

		while (it.hasNext()) {
			JButton button = (JButton) it.next();
			System.out.println("Position is = " + button.getLocation().x + "\n" + button.getLocation().y);
			rightPanel.add(button);
		}
		panel.add(rightPanel, BorderLayout.EAST);
		panel.add(rightPanel);
		rightPanel.add(Box.createVerticalGlue());
		add(rightPanel, BorderLayout.EAST);
		setLabels();
		redirectSystemStreams();
		
	}

	@Override
	public void BTN_LEFT() {
		System.out.println("BTN_LEFT " + getClass().getSimpleName());
		setLabels();

		CardControllerFrame.getInstance().setAction("RFIDMAINFRAME");
		CardControllerFrame.getInstance().setMenu("RFIDMAINFRAME");
	}

	@Override
	public void BTN_LEFT_CENTER() {
		try {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			Keys.getInstance().down();
		} catch (AWTException e) {
			e.printStackTrace();
		}
		System.out.println("BTN_LEFT_CENTER " + getClass().getSimpleName());
	}

	@Override
	public void BTN_CENTER() {
		System.out.println("BTN_CENTER " + getClass().getSimpleName());
		setInfoLbl("f5d5g8", 2000);
	}
	

	@Override
	public void BTN_RIGHT_CENTER() {
		System.out.println("BTN_RIGHT_CENTER " + getClass().getSimpleName());
		CardControllerFrame.getInstance().setFocus();
		try {
			Keys.getInstance().select();
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void BTN_RIGHT() {
		System.out.println("BTN_RIGHT " + getClass().getSimpleName());
		CardControllerFrame.getInstance().setFocus();
		try {
			Keys.getInstance().tab();
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public static Menu getInstance() {
		if (instance == null) {
			instance = new Menu();
		}
		return instance;
	}
}
