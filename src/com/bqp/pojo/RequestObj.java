package com.bqp.pojo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Joby Pooppillikudiyil
 * @Since Mar 17, 2015
 * @Version Athena 1.0
 */
public class RequestObj implements Serializable {

	private static final long serialVersionUID = 1610343072104028982L;
	private String portletId;
	private String requestType;
	private String dataController;
	private Map<String, Object> params=new HashMap<String, Object>();

	public String getPortletId() {
		return portletId;
	}

	public void setPortletId(String portletId) {
		this.portletId = portletId;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	public String getDataController() {
		return dataController;
	}

	public void setDataController(String dataController) {
		this.dataController = dataController;
	}

	@Override
	public String toString() {
		return "RequestObj [portletId=" + portletId + ", requestType=" + requestType + ", dataController=" + dataController + ", params=" + params + "]";
	}
}
