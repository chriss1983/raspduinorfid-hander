package com.bqp.controller;

import java.awt.CardLayout;
import java.awt.Component;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JPanel;

import com.bqp.mainview.CardControllerFrame;
import com.bqp.pojo.UserDetailsEntity;
import com.bqp.utils.CardsList;
import com.bqp.views.menu.Cards;

public class Controller implements Observer {
	public static UserDetailsEntity userDetailsEntity;
	
	public static String action = "";
	public static String viewType = "";
	public static String button = "";
	public static String reason = "";
	private static Controller instance;
	private String serachPattern = "BTN_";
	

	private static CardControllerFrame layoutTest;

	/**
	 * Main loop of the program. either shows a Card Panel, or performs the
	 * method implemented in the card from Panel.
	 */
	private Controller() {
		userDetailsEntity = new UserDetailsEntity();
		userDetailsEntity.setFirstName("Peter");
		userDetailsEntity.setLastName("Maier");
		System.out.println("Create Controller");
		new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("Start While");
				while (true) {
					System.out.print("");
					if (!action.equals("") && !button.equals("")) {
						System.out.println("ACTION: " + action);
						Cards cards = (Cards) CardsList.PAGES.get(action);

						CardLayout cardLayout = (CardLayout) CardControllerFrame.cardPanel.getLayout();
						cardLayout.show(CardControllerFrame.cardPanel, action);
						System.out.println("VIEW: " + cards);
						System.out.println("Button: " + button);
						if (!button.equals("")) {
							try {
								cards.doAction(button);
								button = "";
							} catch (ClassNotFoundException e) {
								e.printStackTrace();
							} catch (NoSuchMethodException e) {
								e.printStackTrace();
							} catch (SecurityException e) {
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (IllegalArgumentException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}

						}
						button = "";
						// action = "";
					}
					
					if (!viewType.equals("")) {
						viewType = viewType.replace("\"", "");
						System.out.println("VIEWTYPE" +viewType);

						if (viewType.equals("LOGIN") | viewType.equals("LOGOUT") & userDetailsEntity != null) {
							System.out.println("COMMAND = " + viewType + "&" + userDetailsEntity.getFirstName() + " " +  userDetailsEntity.getLastName());
							RfidViewController.changeView(viewType + "&" + userDetailsEntity.getFirstName() + " " + userDetailsEntity.getLastName());

							viewType = "";
							userDetailsEntity = null;
//							 userLoginInfoEntity = null;
						} else if (viewType.equals("ERROR")) {
							RfidViewController.changeView(viewType);
							viewType = "";
						}
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						RfidViewController.changeView("DEFAULT");

					}
				}
			}
		}).start();
	}

	/**
	 * update all observer and set the Button and View
	 */
	@Override
	public void update(Observable o, Object action) {
		Pattern p = Pattern.compile(serachPattern);
		Matcher m = p.matcher((String) action);

		if (m.find()) {
			System.out.println("Button " + action + " gedrückt");
			button = (String) action;
			return;
		}
		System.out.println("Change to: " + action);
		Controller.action = (String) action;
	}

	/**
	 * 
	 * @return get instance of this object
	 */
	public static Controller getInstance() {
		if (instance == null) {
			instance = new Controller();
		}
		return instance;
	}

	public void changeView(String card) {

	}

}
