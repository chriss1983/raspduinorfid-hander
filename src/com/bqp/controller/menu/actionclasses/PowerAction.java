package com.bqp.controller.menu.actionclasses;

public class PowerAction {

	public void poweroff() {
		System.out.println("ACTION = POWEROFF");
		System.exit(0);
	}

	public void restart() {
		System.out.println("ACTION = REBOOT");
		System.exit(0);
	}
}
