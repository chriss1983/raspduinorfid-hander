package com.bqp.controller.menu.actionclasses;

import com.bqp.mainview.CardControllerFrame;
import com.bqp.utils.LoadProperties;
import com.bqp.views.menu.Cards;
import com.bqp.views.menu.RfidMainFrame;

public class AbsenceReasonAction {
	private static final long serialVersionUID = 1L;
	
	private final String className = this.getClass().getSimpleName().toLowerCase();

	public void education() {
		System.out.println("HIER IST DIE KLASSE ABSENCEREASONACTION!!");
		CardControllerFrame.getInstance().setAction("LANGUAGESELECTOR");
		CardControllerFrame.getInstance().changeView("LANGUAGESELECTOR");
	}
	
	public void outsideappointment() {
		System.out.println("CLOCKSETTINGS");
		CardControllerFrame.getInstance().setAction("CLOCKSETTINGS");
		CardControllerFrame.getInstance().setMenu("CLOCKSETTINGS");
	}
}
