package com.bqp.controller.menu.actionclasses;

import com.bqp.mainview.CardControllerFrame;
import com.bqp.utils.LoadProperties;
import com.bqp.views.menu.Cards;
import com.bqp.views.menu.RfidMainFrame;

public class MenuAction {
	private static final long serialVersionUID = 1L;
	
	private final String className = this.getClass().getSimpleName().toLowerCase();

	public void changelang() {
		System.out.println("HIER IST DIE KLASSE MENUACTION!!");
		CardControllerFrame.getInstance().setAction("LANGUAGESELECTOR");
		CardControllerFrame.getInstance().changeView("LANGUAGESELECTOR");
	}
	
	public void clocksettings() {
		System.out.println("CLOCKSETTINGS");
		CardControllerFrame.getInstance().setAction("CLOCKSETTINGS");
		CardControllerFrame.getInstance().setMenu("CLOCKSETTINGS");
	}
	
	public void power() {
		CardControllerFrame.getInstance().setAction("POWER");
		CardControllerFrame.getInstance().setMenu("POWER");
	}
	
	/** The Actions for Any Buttons */
	public void setenabled () {
		if(Boolean.parseBoolean(LoadProperties.getInstance().getValue(className, "clocksettings", "clock.setenabled"))) {
			LoadProperties.getInstance().setValue(className, "clocksettings", "clock.setenabled", "false");
		} else {
			LoadProperties.getInstance().setValue(className, "clocksettings", "clock.setenabled", "true");
		}
	}
	
	public void textsize() {
		System.out.println("TEXTSETTINGS");
	}
	
	public void help() {
		System.out.println("HELP");
//		RfidMainFrame.getInstance().setPage("LOGIN&Christian Richter");
		
		/** !!Setter for InfoPage!! */
		CardControllerFrame.getInstance().setPage("DEFAULT");
	}
}
