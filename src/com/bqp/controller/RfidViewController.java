package com.bqp.controller;

import com.bqp.mainview.CardControllerFrame;
import com.bqp.utils.constants.Menus;
import com.bqp.views.menu.RfidMainFrame;
import com.bqp.views.rfid.InfoPages;

/**
 * RaspDuino RFID Time Recording
 * 
 * @author Christian Richter
 * @since Date: 10.06.2015
 * @version 1.0
 * 
 *          Last Edit by: Christian Richter 29.07.2015
 *          Reason: 
 *          -	Added the option to change the menu
 *          -	Add detailed comments
 * 
 *          The Method checks the incoming string after the '&' character and
 *          separates it. The separator is used to send a command for the
 *          display and the name of the user. The area in front of the separator
 *          is to be set and the display area after that to be set name (if
 *          available) The display is set dynamically and can be easily extended
 *          without having to change this method. should not be given separator,
 *          so this is passed without verification, - provided that it is
 *          specified in the interface 'InfoPages' - passed.
 * 
 */
public class RfidViewController {
	public static String viewMode = "DEFAULT";
	public static String settingData;

	/**
	 * 
	 * @param viewMode
	 *            - The page to be displayed
	 */
	public static void changeView(String viewMode) {
		System.out.println("VIEWMODE: " + viewMode);

		/**
		 * Split the incoming String to 'Command' and 'User Firstname -
		 * Lastname'
		 */
		String cmd = null;
		int splitChar = viewMode.indexOf("&");
		if (splitChar != -1) {
			cmd = viewMode.substring(0, splitChar);
		}
		System.out.println("CMD = " + cmd);

		/** constitutes part of the information view dynamically */
		for (int i = 0; i < InfoPages.PAGES.size(); i++) {
			if (viewMode.equals(InfoPages.page[i])) {
				CardControllerFrame.getInstance().setPage(InfoPages.page[i]);
			} else if (cmd != null && cmd.equals("LOGIN") || cmd != null && cmd.equals("LOGOUT")) {
				CardControllerFrame.getInstance().setPage(viewMode);
			}
		}

		/** sets the pages of the menu dynamically */
		for (int i = 0; i < Menus.menu.length; i++) {
			if (viewMode.equals(Menus.menu[i])) {
				CardControllerFrame.getInstance().setPage(viewMode);
			}
		}
	}
	
}
