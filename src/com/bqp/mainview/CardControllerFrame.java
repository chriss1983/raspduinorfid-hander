package com.bqp.mainview;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Observable;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.bqp.controller.Controller;
import com.bqp.utils.CardsList;
import com.bqp.views.menu.RfidMainFrame;
import com.bqp.views.rfid.InfoPages;

public class CardControllerFrame extends Observable {
	// Singleton variable
	// Variable holds an instance of itself
	private static CardControllerFrame instance;
	// The main window
	private JFrame mainFrame;

	public static String action;
	private String main = "";

	private final int width = 800, height = 450; //800, 450

	private JPanel north, south;
	public static JPanel cardPanel;

	private CardControllerFrame() {
		mainFrame = new JFrame("CardLayoutTest");
		north = new JPanel();
		south = new JPanel();
		createMainFrame();
	}

	/**
	 * Initializes the main window
	 */
	private void createMainFrame() {
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setSize(new Dimension(width, height));
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setLayout(new BorderLayout());
		mainFrame.add(getCards(), BorderLayout.CENTER);

		// mainFrame.add(getNorthPanels(north), BorderLayout.NORTH);
		// mainFrame.add(getSouthPanels(south), BorderLayout.SOUTH);
		mainFrame.setVisible(true);
	}

	/**
	 * 
	 * @param label
	 *            the label to be set
	 * @param text
	 *            the text to be set
	 */
	public void setFocus() {
		mainFrame.toFront();
	}
	
	public static void setPage(String page) {
		CardLayout cards = (CardLayout) cardPanel.getLayout();
		cards.show(cardPanel, page);
	}

	/**
	 * add Logo Button
	 * 
	 * @param jPanel
	 * @return
	 */
	private Component getNorthPanels(JPanel jPanel) {
		JButton b = new JButton("LOGOBUTTON");
		jPanel.setLayout(new GridLayout(0, 1));
		jPanel.setBackground(Color.CYAN);
		jPanel.add(b);
		return jPanel;
	}

	/**
	 * 
	 * @return add registered Cards
	 */
	private JPanel getCards() {
		cardPanel = new JPanel(new CardLayout());
		for (int i = 0; i < CardsList.PAGES.size(); i++) {
			cardPanel.add(CardsList.page[i], (JPanel) CardsList.PAGES.get(CardsList.page[i]));
		}

		for (int i = 0; i < InfoPages.getPages().size(); i++) {
			cardPanel.add((InfoPages.page[i]), (JPanel) InfoPages.PAGES.get(InfoPages.page[i]));
			RfidMainFrame.cardLayout.add((InfoPages.page[i]), (JPanel) InfoPages.PAGES.get(InfoPages.page[i]));
		}
		return cardPanel;
	}

	/**
	 * Displays a card or panel directs action further to the CardLayout
	 * {@code setMenu(String)} Displays the CardLayout Panel
	 * {@code setAction (String)} Redirects to the selected action CardLayout
	 * further
	 * 
	 */
	public void makeAction(String action) {

		switch (action) {
		case "BTN_LEFT":

			setAction(action);
			break;

		case "BTN_LEFT_CENTER":
			// setMenu("MENU");
			setAction(action);
			break;

		case "BTN_CENTER":
			// setMenu("TESTCARD4");
			setAction(action);
			break;

		case "BTN_RIGHT_CENTER":
			if (getCurrentCard().equals("RFIDMAINFRAME")) {
				System.out.println("###RFIDMAINFRAME###");
				setMenu("ABSENCEREASON");
				// main = "MENU";
			} else {
				// setMenu("MENU");
				setAction(action);
			}
			break;

		case "BTN_RIGHT":
			// if (getCurrentCard().equals("MENU")) {
			// System.out.println("###MENU###");
			// setMenu("LANGUAGESELECTOR");
			// // main="";
			// } else
			if (getCurrentCard().equals("RFIDMAINFRAME")) {
				System.out.println("###RFID###");
				setMenu("MENU");
				// main = "MENU";
			} else {
				// setMenu("MENU");
				setAction(action);
			}
			break;

		default:
			break;
		}
	}

	/**
	 * set the Menu and the Button
	 */
	public void setMenu(String menu) {
		// Cards.getInstance().setLabels();
		setAction(menu);
		changeView(menu);
	}

	/**
	 * Notify all Observers
	 */
	public void setAction(String action) {
		this.action = action;
		setChanged();
		notifyObservers(action);
	}

	/**
	 * Change the Current CardLayout to Selected
	 */
	public void changeView(String card) {
		CardLayout cards = (CardLayout) cardPanel.getLayout();
		cards.show(cardPanel, card);
	}

	/**
	 * 
	 */
	public static void changeButtonName(String buttonName, String newText) {
		// if()
		// buttonLeftCenter, buttonCenter, buttonRightCenter, buttonRight;
	}

	/**
	 * 
	 * @return displays the current card Panel
	 */
	public String getCurrentCard() {
		JPanel card = null;
		for (Component comp : cardPanel.getComponents()) {
			if (comp.isVisible() == true) {
				card = (JPanel) comp;
				System.out.println(card.getName());
			}
		}
		String current = card.getClass().getSimpleName().toUpperCase();
		System.out.println("Current Card = " + current);
		if (current.equals("RFIDMAINFRAME")) {
			System.out.println("Current is Activated");
		}

		return current;
	}

	public void restart() {
		mainFrame.dispose();
		new CardControllerFrame();
		this.addObserver(Controller.getInstance());
	}

	/**
	 * 
	 * @return Singelton Instance of this Object
	 */
	public static CardControllerFrame getInstance() {
		if (instance == null) {
			instance = new CardControllerFrame();
		}
		return instance;
	}
}
